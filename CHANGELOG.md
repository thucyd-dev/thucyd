# Changelog

## 0.2.6 (2024-04-27)

Docstring fixes for correctness and to address problematic hover rendering.


## 0.2.5 (2024-01-27)

Revised the implementation of the arctan2 angle solver to account for sparsity. There is now a fast path for density vectors and a slow path for sparse vectors.

Revised floating point comparisons to compare abs value with eps. 

Harmonized the full-dimension size for all unit tests.


## 0.2.4 (2024-01-02)

Docstring and url fixes.


## 0.2.3 (2023-12-28)

Adds a second method to orient an eigenbasis. The new method uses the modified `arctan2` method and is currently undocumented. The defaults on the `orient_eigenvectors` function lead to a call of the version 0.2.1 implementation, so no change will be noticed.

There is now a `FutureWarning` on the shape of input eigenvalues `E`: while this version continues to allow square-matrix `E`, in the future `E` must be a vector. 


## 0.1.2 (2019-07-08)

First release. See [A consistently oriented basis for eigenanalysis](https://link.springer.com/article/10.1007/s41060-020-00227-z) for theory and implementation.
