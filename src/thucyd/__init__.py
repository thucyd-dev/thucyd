"""
-------------------------------------------------------------------------------

`thucyd` package top-level import

-------------------------------------------------------------------------------
"""

# AUTO-GENERATED version
__version__ = "0.2.7"

# load eigen subpackage into thucyd package namespace
from . import eigen

# declare public API subpackages
__all__ = ["eigen"]
