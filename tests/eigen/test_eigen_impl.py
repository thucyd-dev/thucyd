"""
-------------------------------------------------------------------------------

Unit tests for the .impl module of thucyd.eigen.

-------------------------------------------------------------------------------
"""

import unittest
import numpy as np

from src.thucyd.eigen import impl

np.set_printoptions(precision=3, suppress=True)


# noinspection SpellCheckingInspection,PyPep8Naming
class TestEigenImpl(unittest.TestCase):
    """
    Unit tests for the eigen.basis module.
    """

    def setUp(self):
        self._full_dim_max = 21  # 11
        self._full_dim_min = 2
        self._precision = 6
        self._rebase = np.power(10, self._precision)

    def test_hangle_eigenvalue_input_cases(self):
        """
        Tests that a vector is returned regardless of whether a vector
        or matrix is passed.
        """

        # vector case
        E_vec = np.ones(7)
        E_vec_handled, is_vector = impl.handle_eigenvalue_input(E_vec)

        with self.subTest(msg=f"case: handle vector"):
            self.assertEqual(E_vec_handled.ndim, 1)
            self.assertTrue(is_vector)

        # matrix case
        E_mtx = np.diag(np.arange(7))
        E_mtx_handled, is_vector = impl.handle_eigenvalue_input(E_mtx)

        with self.subTest(msg=f"case: handle matrix"):
            self.assertEqual(E_mtx_handled.ndim, 1)
            self.assertFalse(is_vector)

    def test_sort_eigenvectors_are_descending_for_nonequal_eigenvalues(
        self,
    ):
        """
        Confirm that `sort_eigenvectors` correctly sorts given an input of
        randomly ordered and signed eigenvalues.
        """

        # setup dimensions
        dims = np.arange(self._full_dim_min, self._full_dim_max)

        # set seed
        np.random.seed(1)

        # iter through dimensions
        for dim in dims[:1]:
            # setup V, E
            #   V = [[0, 1, 2, 3, ...
            #        [0, 1, 2, 3, ...
            cols = np.arange(dim)
            V = np.repeat(cols[np.newaxis, :], dim, axis=0)

            # E with random (albeit reproducible) signed values
            E = np.random.uniform(-10, 10, dim)

            # call
            Vsort, Esort, sort_indices = impl.sort_eigenvectors(V, E)

            # validate sort is descending on abs(E)
            e_abs = np.fabs(Esort)
            self.assertTrue(
                np.all(np.array(e_abs[i] >= e_abs[i + 1] for i in range(dim)))
            )

            # validate sort vector is as expected
            answer = np.argsort(np.fabs(E))[::-1]
            self.assertSequenceEqual(list(sort_indices), list(answer))

            # validate Esort is sorted
            answer = E[sort_indices]
            self.assertSequenceEqual(
                np.round(self._rebase * Esort).tolist(),
                np.round(self._rebase * answer).tolist(),
            )

    def test_sort_eigenvectors_are_unalterned_for_equal_eigenvalues(self):
        """
        Confirm that `sort_eigenvectors` does not change the order of
        eigenvalues when the eigenvalues are all equal.
        """

        # setup dimensions
        dims = np.arange(self._full_dim_min, self._full_dim_max)
        amplitude = 1e-8

        # set seed
        np.random.seed(1)

        # iter through dimensions
        for dim in dims:
            # setup V, E
            #   V = [[0, 1, 2, 3, ...
            #        [0, 1, 2, 3, ...
            cols = np.arange(dim)
            V = np.repeat(cols[np.newaxis, :], dim, axis=0)

            E = amplitude * np.random.normal(0, 1, size=dim)

            # call
            Vsort, Esort, sort_indices = impl.sort_eigenvectors(V, E)

            # validate that sort indices are unchanged
            self.assertSequenceEqual(list(sort_indices), list(np.arange(dim)))

    def test_make_givens_rotation_matrix_in_subspace_across_cursors(self):
        """
        Validates that a proper Givens matrix is constructed by testing
        that the determinant of the four Givens points is one, as expected for
        a rotation matrix.
        """

        # setup dimensions
        full_dim = 10

        # setup cursor array
        cursors = np.arange(full_dim)

        # iterate on cursors
        for cursor in cursors:
            # setup sub-cursors array
            sub_cursors = np.arange(cursor + 1, full_dim)

            # iterate on sub-cursors
            for sub_cursor in sub_cursors:
                # run-test: make a Givens matrix
                R = impl.make_givens_rotation_matrix_in_subspace(
                    full_dim, cursor, sub_cursor, np.pi / 6.0
                )

                a = R[cursor, cursor]
                b = R[cursor, sub_cursor]
                c = R[sub_cursor, cursor]
                d = R[sub_cursor, sub_cursor]
                det = a * d - b * c

                # test
                self.assertAlmostEqual(det, 1.0)

    def test_construct_subspace_rotation_matrix_matches_sweep_expectation(
        self,
    ):
        """
        Tests the basis.construct_subspace_rotation_matrix function for the
        case where Givens rotation angles sweep from (-90: 90) deg uniformly
        based on the size of the current sub-dimension.

        Notes
        -----

        Tests the focus function by building up a rotation matrix R from
        locally-held angles and then testing that the matrix is built by the
        focus fcxn.

        Since the calculation of R is essentially mechanical, this test simply
        confirms that the called function operates as expected.

        The rotation angles follow the pattern

            sub-dim 1: [0]
            sub-dim 2: [-45, 45]
            sub-dim 3: [-60, 0, 60]
            ...
        """

        # setup dimensions
        full_dims = np.arange(self._full_dim_min, self._full_dim_max)

        # test-setup: iterate on full dimensions
        for full_dim in full_dims:
            # setup cursor array
            cursors = np.arange(full_dim)

            # test-setup: iterate on cursors
            for cursor in cursors:
                # setup sub-cursors array
                sub_cursors = np.arange(cursor + 1, full_dim)

                # setup test angles as vector in full-dim
                angles_test = np.zeros(full_dim)
                num_angles = angles_test[cursor + 1 :].shape[0]

                # uniformly distribute angles on (-90, 90) centered at zero
                def angles_gen(n):
                    if n > 0:
                        return (
                            np.array([x for x in (np.pi / n) * np.arange(n)])
                            - np.pi / 2.0 * (n - 1) / n
                        )
                    else:
                        return np.array([])

                angles_test[cursor + 1 :] = angles_gen(num_angles)

                R_test = np.eye(full_dim)

                # test-setup: calculate R explicitly
                for sub_cursor in sub_cursors[::-1]:
                    # noinspection PyUnresolvedReferences
                    R_test = impl.make_givens_rotation_matrix_in_subspace(
                        full_dim, cursor, sub_cursor, angles_test[sub_cursor]
                    ).dot(R_test)

                # run-test: make call to focus function
                R_response = impl.construct_subspace_rotation_matrix(
                    full_dim, cursor, angles_test
                )

                # test element wise
                with self.subTest(
                    msg="test element wise - fdim {0} sdim {1}".format(
                        full_dim, full_dim - cursor + 1
                    )
                ):
                    self.assertSequenceEqual(
                        np.round(self._rebase * R_response).tolist(),
                        np.round(self._rebase * R_test).tolist(),
                    )

                # test against identity
                I_from_RR = R_response.T.dot(R_test)

                with self.subTest(
                    msg="test R'R == identity - fdim {0} sdim {1}".format(
                        full_dim, full_dim - cursor + 1
                    )
                ):
                    self.assertSequenceEqual(
                        np.round(self._rebase * I_from_RR).tolist(),
                        np.round(self._rebase * np.eye(full_dim)).tolist(),
                    )

    def test_solve_rotation_angles_in_subdimension_via_arctan2_for_vcol_eq_inv_sqrt_n(
        self,
    ):
        """
        Test `solve_rotation_angles_in_subdimension_via_arctan2` for correct angles when
        all elements of `Vcol` are set to 1 / sqrt(full-dim), for a range of
        `full_dim`s.
        """

        # setup
        full_dims = np.arange(self._full_dim_min, self._full_dim_max)
        cursor = 0

        # iter on dimension
        for full_dim in full_dims:
            # setup Vcol
            Vcol = np.ones(full_dim)
            Vcol /= np.linalg.norm(Vcol)

            # use the arcsin method to compute angles
            v = Vcol[0]
            sub_cursors = np.arange(cursor + 1, full_dim)
            angles_work = np.zeros(full_dim + 1)
            r = 1.0

            for sub_cursor in sub_cursors[::-1]:
                r *= np.cos(angles_work[sub_cursor + 1])
                angles_work[sub_cursor] = np.arcsin(v / r)

            angles_answer = angles_work[:full_dim]

            # run-test: call focus fcxn (which uses modified arctan2 method)
            angles_test = (
                impl.solve_rotation_angles_in_subdimension_via_arctan2(
                    full_dim, cursor, Vcol
                )
            )

            # reconstruct `Vcol`
            angles_matrix = np.zeros((full_dim, full_dim))
            angles_matrix[0, :] = angles_test
            V_matrix = impl.generate_oriented_eigenvectors(
                angles_matrix, kth_eigenvector=1
            )
            Vcol_reconstruct = V_matrix[:, 0]

            # test element wise
            with self.subTest(msg=f"dim: {full_dim} -- angles"):
                self.assertSequenceEqual(
                    np.round(self._rebase * angles_test).tolist(),
                    np.round(self._rebase * angles_answer).tolist(),
                )
            with self.subTest(msg=f"dim: {full_dim} -- vector"):
                self.assertSequenceEqual(
                    np.round(self._rebase * Vcol).tolist(),
                    np.round(self._rebase * Vcol_reconstruct).tolist(),
                )

    def test_solve_rotation_angles_in_subdimension_via_arctan2_for_irreducible_dimension(
        self,
    ):
        """
        Test `solve_rotation_angles_in_subdimension_via_arctan2` when the system is
        irreducible, which is when cursor == full_dim - 1. The solution is that
        all angles are zero.
        """

        # setup
        full_dims = np.arange(self._full_dim_min, self._full_dim_max)

        # iter on dimension
        for full_dim in full_dims:
            # fix cursor
            cursor = full_dim - 1

            # setup Vcol
            Vcol = np.ones(full_dim)
            Vcol /= np.linalg.norm(Vcol)

            # answer
            angles_answer = np.zeros(full_dim)

            # run-test: call focus fcxn
            angles_test = (
                impl.solve_rotation_angles_in_subdimension_via_arctan2(
                    full_dim, cursor, Vcol
                )
            )

            # test element wise
            with self.subTest(msg=f"dim: {full_dim}"):
                self.assertSequenceEqual(
                    np.round(self._rebase * angles_test).tolist(),
                    np.round(self._rebase * angles_answer).tolist(),
                )

    def test_solve_rotation_angles_in_subdimension_via_arctan2_for_vcol_with_trailing_zeros(
        self,
    ):
        """
        Tests that Vcol vectors like [1/sqrt(2), 1/sqrt(2), 0, 0, ..., -0]
        return angle arrays [0, pi/4, 0, 0, ..., 0]. This Vcol was a catastrophic
        edge case in the original arctan2 method, but is repaired when using the
        modified arctan2 method.
        """

        # setup
        full_dims = np.arange(3, self._full_dim_max)
        cursor = 0

        # iter on dimension
        for full_dim in full_dims:
            # setup Vcol
            Vcol = np.zeros(full_dim)
            Vcol[0] = 1 / np.sqrt(2)
            Vcol[1] = 1 / np.sqrt(2)
            Vcol[-1] *= -1  # IEEE negative zero

            # setup answer
            angles_answer = np.zeros_like(Vcol)
            angles_answer[1] = np.pi / 4

            # run-test: call the focus fcxn
            angles_test = (
                impl.solve_rotation_angles_in_subdimension_via_arctan2(
                    full_dim, cursor, Vcol
                )
            )

            with self.subTest(msg=f"dim: {full_dim}"):
                self.assertSequenceEqual(
                    np.round(self._rebase * angles_test).tolist(),
                    np.round(self._rebase * angles_answer).tolist(),
                )

    def test_solve_rotation_angles_in_subdimension_via_arctan2_with_interstitial_zeros(
        self,
    ):
        """
        The modified arctan2 method skip elements in a column vector that are
        zero, while marking the associated angles with theta = 0. For instance,
        with

            Vcol -> [a1, a2, 0, a4]

        the method needs to solve the a4 / a2 equation after marking theta_3 = 0.

        To implement this for the general case, two cursors are used to walk down
        a column vector: there's a front cursor and a back cursor.

                      |  a1  |
                      |  a2  | <-- back cursor
            Vcol ->   |  0   |
                      |  a4  | <-- front cursor
                      |  a5  |

        Elements that are zero are called defects.
        """

        def reconstruct_Vcol(full_dim_: int, angles_: np.ndarray) -> np.ndarray:
            """helper that handles wrapping and unwrapping"""
            angles_matrix_ = np.zeros((full_dim_, full_dim_))
            angles_matrix_[0, :] = angles_
            return impl.generate_oriented_eigenvectors(
                angles_matrix_, kth_eigenvector=1
            )[:, 0]

        # setup
        full_dims = np.arange(3, self._full_dim_max)
        cursor = 0

        # one defect
        n_defect = 1
        for full_dim in full_dims:
            with self.subTest(msg=f"dim: {full_dim}, defects: {n_defect}"):
                for i_defect in np.arange(1, full_dim - 1):
                    Vcol = np.ones(full_dim)
                    Vcol /= np.sqrt(full_dim - n_defect)
                    Vcol[0] *= -1
                    Vcol[i_defect] = 0.0

                    # run-test: call focus fcxn
                    angles_test = (
                        impl.solve_rotation_angles_in_subdimension_via_arctan2(
                            full_dim, cursor, Vcol
                        )
                    )
                    Vcol_reconstruct = reconstruct_Vcol(full_dim, angles_test)

                    # test
                    self.assertSequenceEqual(
                        np.round(self._rebase * Vcol).tolist(),
                        np.round(self._rebase * Vcol_reconstruct).tolist(),
                    )

        # two defects
        n_defect = 2
        for full_dim in full_dims[1:]:
            with self.subTest(msg=f"dim: {full_dim}, defects: {n_defect}"):
                for i_defect in np.arange(1, full_dim - 1):
                    for j_defect in np.arange(i_defect + 1, full_dim - 1):
                        Vcol = np.ones(full_dim)
                        Vcol /= np.sqrt(full_dim - n_defect)
                        Vcol[0] *= -1
                        Vcol[i_defect] = 0.0
                        Vcol[j_defect] = 0.0

                        # run-test: call focus fcxn
                        angles_test = impl.solve_rotation_angles_in_subdimension_via_arctan2(
                            full_dim, cursor, Vcol
                        )
                        Vcol_reconstruct = reconstruct_Vcol(
                            full_dim, angles_test
                        )

                        # test
                        self.assertSequenceEqual(
                            np.round(self._rebase * Vcol).tolist(),
                            np.round(self._rebase * Vcol_reconstruct).tolist(),
                        )

        # three defects
        n_defect = 3
        for full_dim in full_dims[1:]:
            with self.subTest(msg=f"dim: {full_dim}, defects: {n_defect}"):
                for i_defect in np.arange(1, full_dim - 1):
                    for j_defect in np.arange(i_defect + 1, full_dim - 1):
                        for k_defect in np.arange(j_defect + 1, full_dim - 1):
                            Vcol = np.ones(full_dim)
                            Vcol /= np.sqrt(full_dim - n_defect)
                            Vcol[0] *= -1
                            Vcol[i_defect] = 0.0
                            Vcol[j_defect] = 0.0
                            Vcol[k_defect] = 0.0

                            # run-test: call focus fcxn
                            angles_test = impl.solve_rotation_angles_in_subdimension_via_arctan2(
                                full_dim, cursor, Vcol
                            )
                            Vcol_reconstruct = reconstruct_Vcol(
                                full_dim, angles_test
                            )

                            # test
                            self.assertSequenceEqual(
                                np.round(self._rebase * Vcol).tolist(),
                                np.round(
                                    self._rebase * Vcol_reconstruct
                                ).tolist(),
                            )

    def test_solve_rotation_angles_in_subdimension_via_arctan2_over_sequence_of_entry_signs(
        self,
    ):
        """
        To exercise the calculation of major angles as well as the sign of minor
        angles, a positive `Vcol` has its entries flipped in sign (one at a time),
        after which the vector of angles is calculated.

        In brief, for the map Vcol = [+, +, +, ...] -> angles = [0, a1, a2, ...],
        there are two parts to this test:

            Vcol -> [-, +, +, ...]  -->  angles -> [0, a1 + 90, a2, ...]

        and

            Vcol -> [+, ..., +, -, +, ...]  -->  angles -> [0, ..., ai, -aj, ak, ...].

        Put another way, a leading minus sign results the first angle being +90 deg
        from the original. All other sign flips are one-for-one positional.
        """

        # setup
        full_dims = np.arange(4, self._full_dim_max)
        cursor = 0

        # iter on dimension
        for full_dim in full_dims:
            with self.subTest(msg=f"full_dim: {full_dim}"):
                # setup
                Vcol = np.ones(full_dim)
                Vcol /= np.linalg.norm(Vcol)

                # get template of angle for this all-positive vector
                angles_template = (
                    impl.solve_rotation_angles_in_subdimension_via_arctan2(
                        full_dim, cursor, Vcol
                    )
                )

                # setup element-wise sign-flip vector
                signs = np.ones_like(Vcol)
                signs[0] = -1

                # run-test: call focus fcxn with this first configuration
                angles_test = (
                    impl.solve_rotation_angles_in_subdimension_via_arctan2(
                        full_dim, cursor, Vcol * signs
                    )
                )
                self.assertAlmostEqual(
                    angles_template[1] + np.pi / 2,
                    angles_test[1],
                    places=self._precision,
                )

                for i in np.arange(1, signs.shape[0]):
                    signs = np.roll(signs, 1)

                    # run-test: call focus fcxn with subsequent configurations
                    angles_test = (
                        impl.solve_rotation_angles_in_subdimension_via_arctan2(
                            full_dim, cursor, Vcol * signs
                        )
                    )
                    self.assertAlmostEqual(
                        -angles_template[i],
                        angles_test[i],
                        places=self._precision,
                    )

    def test_solve_rotation_angles_in_subdimension_via_arcsin_for_const_vcol(
        self,
    ):
        """
        Test `solve_rotation_angles_in_subdimension_via_arcsin` in the case
        where the elements of `Vcol` are the same and set to 1 / sqrt(full-dim).

        Notes
        -----
        The calculation of angles and the organization of (full-dim, cursor)
        tuples are different concerns. Here, the embedded angles from a single
        full dimension are calculated theoretically and via test, and the two
        angle vectors are compared element by element for a near match.
        """

        # setup
        full_dim = 20
        cursor = 0

        # work through theoretic answer
        v = 1.0 / np.sqrt(full_dim)
        sub_cursors = np.arange(cursor + 1, full_dim)
        angles_work = np.zeros(full_dim + 1)
        r = 1.0

        for sub_cursor in sub_cursors[::-1]:
            r *= np.cos(angles_work[sub_cursor + 1])
            angles_work[sub_cursor] = np.arcsin(v / r)

        angles_answer = angles_work[:full_dim]

        # setup for test call
        Vcol = np.ones(full_dim)
        Vcol /= np.linalg.norm(Vcol)

        # run-test: call focus fcxn
        angles_response = impl.solve_rotation_angles_in_subdimension_via_arcsin(
            full_dim, cursor, Vcol
        )

        # test element wise
        self.assertSequenceEqual(
            np.round(self._rebase * angles_response).tolist(),
            np.round(self._rebase * angles_answer).tolist(),
        )

    def test_reduce_dimension_by_one_from_full_space(self):
        """
        Test `reduce_dimension_by_one` to ensure that the subspace dimension is
        indeed reduced by one. This test treats only the subspace == full space
        case.
               -            -     -            -
               | *  *  *  * |     | 1          |
        R.T x  | *  *  *  * |  =  |    *  *  * |
               | *  *  *  * |     |    *  *  * |
               | *  *  *  * |     |    *  *  * |
               -            -     -            -

        A 1 must appear on the upper-left pivot.
        """

        # setup dimensions
        full_dims = np.arange(self._full_dim_min, self._full_dim_max)
        cursor = 0  # only the case of the full space is considered here

        # test-setup: iterate on full dimensions
        for full_dim in full_dims:
            # setup a mock eigenvector
            A = np.eye(full_dim)
            A[:, 0] = 1.0 / np.sqrt(full_dim)
            V_test, _ = np.linalg.qr(A)

            # call focus fcxn
            V_test, _ = impl.reduce_dimension_by_one(full_dim, cursor, V_test)

            # test the first pivot
            self.assertAlmostEqual(
                V_test[cursor, cursor].item(), 1.0, places=self._precision
            )

    def test_reduce_dimension_by_one_across_all_reducible_subspaces(self):
        """
        Test `reduce_dimension_by_one` to ensure that all reducible subspaces
        are indeed reduced. Concretely, this means that a 1 appears on the
        pivot of the current subspace. The exception is when the space is
        irreducible -- in this case the lowest right element is +/- 1, where
        the sign is the same as the determinant of the original eigenvector
        matrix.

        This test is an extension of the simpler
        `test_reduce_dimension_by_one_at_full_dimension` test.

        Reducible spaces:
               -            -     -            -
               | *  *  *  * |     | 1          |
        R.T x  | *  *  *  * |  =  |    *  *  * | ,
               | *  *  *  * |     |    *  *  * |
               | *  *  *  * |     |    *  *  * |
               -            -     -            -

               -            -     -            -
               | 1          |     | 1          |
        R'.T   |    *  *  * |  =  |    1       |
               |    *  *  * |     |       *  * |
               |    *  *  * |     |       *  * |
               -            -     -            -

        and so on. An irreducible space (here in 4D) looks like

                -                -
                | 1              |
                |    1           |
                |       1        |
                |          +/- 1 |
                -                -

        This case is tested here but as a separate case.
        """

        # setup dimensions
        full_dims = np.arange(self._full_dim_min, self._full_dim_max)

        # test-setup: iterate on full dimensions
        for full_dim in full_dims:
            # setup a mock eigenvector
            A = np.eye(full_dim)
            A[:, 0] = 1.0 / np.sqrt(full_dim)
            V_test, _ = np.linalg.qr(A)
            V_orig = V_test.copy()

            # setup cursor array
            cursors = np.arange(full_dim)

            # test-setup: iterate on cursors
            for cursor in cursors:
                # call focus fcxn
                V_test, angles_col = impl.reduce_dimension_by_one(
                    full_dim, cursor, V_test
                )

                if cursor < cursors[-1]:
                    with self.subTest(
                        msg=f"reducible -- dim: {full_dim}, cursor: {cursor}"
                    ):
                        # test the first pivot
                        self.assertAlmostEqual(
                            V_test[cursor, cursor].item(),
                            1.0,
                            places=self._precision,
                        )

                else:
                    with self.subTest(msg=f"irreducible -- dim: {full_dim}"):
                        # test the last diagonal element
                        self.assertAlmostEqual(
                            V_test[cursor, cursor].item(), np.linalg.det(V_orig)
                        )

    def test_orient_eigenvectors_via_arctan2_method_for_return_results(
        self,
    ):
        """Performs multiple tests on `orient_eigenvectors_via_modified_arctan2_method`

        The tests are:
              i) det(Vor) == 1
             ii) sign_flip_response = [s, 1, ..., s x det(V)]
                    s = 1 if not orient_to_first_orthant else -1
            iii) norm(Vor.dot(R.T) - eye) ~= 0.0
        """

        # setup dimensions
        full_dims = np.arange(self._full_dim_min, self._full_dim_max)

        # test-setup: iterate on full dimensions
        for full_dim in full_dims:
            # setup a mock eigenvector and eigenvalue mtx
            A = np.eye(full_dim)
            A[:, 0] = 1.0 / np.sqrt(full_dim)
            V, _ = np.linalg.qr(A)
            E = np.arange(full_dim)[::-1]

            # iterate on orientation switch
            for orientation_switch in [False, True]:
                # run-test: call focus fcxn
                (
                    Vor,
                    Eor,
                    sign_flip_response,
                    theta_matrix,
                    sort_indices,
                ) = impl.orient_eigenvectors_via_modified_arctan2_method(
                    V, E, orient_to_first_orthant=orientation_switch
                )

                # determinant aspect
                det_Vor_answer = 1.0
                det_Vor_test = np.linalg.det(Vor)

                # sign-flip aspect
                switch_sign = 1 if not orientation_switch else -1
                sign_flip_answer = np.ones(full_dim, dtype=int)
                sign_flip_answer[0] = switch_sign
                sign_flip_answer[-1] = switch_sign * np.sign(np.linalg.det(V))

                # construct the rotation matrix from the angles
                R = impl.generate_oriented_eigenvectors(theta_matrix)

                # and rotate Vor onto eye
                eye_test = Vor.dot(R.T)
                eye_answer = np.eye(full_dim)

                # tests
                with self.subTest(
                    msg=f"dim: {full_dim}, orient: {orientation_switch} -- det(Vor) test"
                ):
                    self.assertAlmostEqual(
                        det_Vor_test, det_Vor_answer, places=self._precision
                    )

                with self.subTest(
                    msg=f"dim: {full_dim}, orient: {orientation_switch}  -- sign-flip test"
                ):
                    self.assertSequenceEqual(
                        list(sign_flip_response), list(sign_flip_answer)
                    )

                with self.subTest(
                    msg=f"dim: {full_dim}, orient: {orientation_switch}  -- orientation test"
                ):
                    self.assertAlmostEqual(
                        np.linalg.norm(eye_test - eye_answer),
                        0.0,
                        places=self._precision,
                    )

    def test_orient_eigenvectors_via_arcsin_method_for_expected_sign_flips_over_dimensions(
        self,
    ):
        """
        Test `orient_eigenvectors_via_arcsin_method` for expected sign flips
        given a mock eigenvector matrix.

        This test is end-to-end in that a test eigenvector matrix is passed
        into the focus function and the resultant sign-flip vector is tested.
        This test is not so strong because the expected sign-flip vector
        has no theoretical importance but is only the result of previous
        observation. In this sense, this unit test is more like a regression
        test.
        """

        # setup dimensions
        full_dims = np.arange(2, self._full_dim_max)

        # test-setup: iterator on full dimensions
        for full_dim in full_dims:
            # setup a mock eigenvector and eigenvalue mtx
            A = np.eye(full_dim)
            A[:, 0] = 1.0 / np.sqrt(full_dim)
            V, _ = np.linalg.qr(A)
            E = np.arange(full_dim)[::-1]

            # run-test: call focus fcxn
            (
                Vor,
                Eor,
                sign_flip_response,
                theta_matrix,
                sort_indices,
            ) = impl.orient_eigenvectors_via_arcsin_method(V, E)

            # make expected flip vector (answer found from cmdline run,
            # therefore this is more of a regression test than a unit test)
            sign_flip_answer = -1.0 * np.ones(full_dim)
            sign_flip_answer[-1] = 1.0

            self.assertSequenceEqual(
                list(sign_flip_response), list(sign_flip_answer)
            )

    def test_orient_eigenvectors_via_arcsin_method_when_sign_flips_are_forced(
        self,
    ):
        """
        Similar to test_orient_eigenvectors_for_expected_sign_flips-
        _over_dimensions above but here column signs at the input are flipped
        by hand. The expectation is that the `orient_eigenvectors` call
        discovers these flips and reports them accordingly.
        """

        # setup dimensions
        full_dims = np.arange(2, self._full_dim_max)

        # test-setup: iterator on full dimensions
        for full_dim in full_dims:
            # setup a mock eigenvector and eigenvalue mtx
            A = np.eye(full_dim)
            A[:, 0] = 1.0 / np.sqrt(full_dim)
            V, _ = np.linalg.qr(A)
            E = np.arange(full_dim)[::-1]

            # make expected flip vector (answer found from cmdline run,
            # therefore this is more of a regression test than a unit test)
            sign_flip_answer = -1.0 * np.ones(full_dim)
            sign_flip_answer[-1] = 1.0

            # setup cursor array
            cursors = np.arange(full_dim - 1)

            # test-setup: iterate on cursors
            for cursor in cursors[::-1]:
                # force a flip on the cursor column
                V[:, cursor] *= -1.0

                # update the expected answer
                sign_flip_answer[cursor] *= -1.0

                # run-test: call focus fcxn
                (
                    _,
                    _,
                    sign_flip_response,
                    _,
                    _,
                ) = impl.orient_eigenvectors_via_arcsin_method(V, E)

                self.assertSequenceEqual(
                    list(sign_flip_response), list(sign_flip_answer)
                )

    def test_orient_eigenvectors_warns_on_eval_matrix_interface(self):
        """Tests that passing an eigenvalue matrix raises a warning"""

        # setup
        V_mtx = np.eye(4)
        E_vec = np.arange(4)[::-1]
        E_mtx = np.diag(E_vec)

        # call to raise warning
        with self.subTest(msg="test that FutureWarning is raised on matrix"):
            with self.assertWarns(FutureWarning):
                impl.orient_eigenvectors(V_mtx, E_mtx)

        # call that does not raise warning
        with self.subTest(
            msg="test that FutureWarning is not raised on vector"
        ):
            try:
                with self.assertWarns(FutureWarning):
                    impl.orient_eigenvectors(V_mtx, E_vec)
            except AssertionError:
                pass
            else:
                self.fail(
                    "A vector of eigenvalues should not have raised a FutureWarning"
                )

    def test_orient_eigenvectors_errors_on_unknown_method(self):
        """Tests that an unknown method raises a RuntimeError"""

        # setup
        V = np.eye(4)
        E = np.arange(4)[::-1]

        # call to raise error
        with self.assertRaises(RuntimeError):
            impl.orient_eigenvectors(V, E, method="unknown")

    def test_orient_eigenvectors_executes_with_known_methods(self):
        """Simple test for no-fault using the arcsin method"""

        # setup
        V = np.eye(4)
        E = np.arange(4)[::-1]

        # tests
        with self.subTest(msg="arcsin method"):
            try:
                with self.assertRaises(Exception):
                    impl.orient_eigenvectors(V, E, method="arcsin")
            except AssertionError:
                pass
            else:
                self.fail(
                    "An exception was unexpectedly raised for the arcsin method"
                )

        # tests
        with self.subTest(msg="arctan2 method"):
            try:
                with self.assertRaises(Exception):
                    impl.orient_eigenvectors(V, E, method="arctan2")
            except AssertionError:
                pass
            else:
                self.fail(
                    "An exception was unexpectedly raised for the arctan2 method"
                )

    def test_generate_oriented_eigenvectors_over_dimensions(self):
        """
        Compares two oriented eigenvector bases:

        (1)    Vor = V S

        and

        (2)    Vor = R .

        Representation (1) comes from the return of `orient_eigenvectors`,
        tested above, and (2) comes from `generate_oriented_eigenvectors`,
        the focus function for this test.
        """

        # setup dimensions
        full_dims = np.arange(2, self._full_dim_max)

        # test-setup: iterator on full dimensions
        for full_dim in full_dims:
            # setup a mock eigenvector and eigenvalue mtx
            A = np.eye(full_dim)
            A[:, 0] = 1.0 / np.sqrt(full_dim)
            V, _ = np.linalg.qr(A)
            E = np.arange(full_dim)[::-1]

            # test-setup: cast into an oriented basis
            (
                Vor_answer,
                _,
                _,
                theta_matrix,
                sort_indices,
            ) = impl.orient_eigenvectors_via_arcsin_method(V, E)

            # run-test: call focus fcxn
            Vor_response = impl.generate_oriented_eigenvectors(theta_matrix)

            # test element wise
            with self.subTest(msg="test dimension: {0}".format(full_dim)):
                self.assertSequenceEqual(
                    np.round(self._rebase * Vor_response).tolist(),
                    np.round(self._rebase * Vor_answer).tolist(),
                )

    def test_generate_oriented_eigenvectors_kth_over_dimensions(self):
        """
        A separate test confirmed that Vor = V S = R. To generate R, a
        cascade of rotation matrices was built.

        When called with a particular index (kth index),

            generate_oriented_eigenvectors(angles_matrix, k)

        returns the rotation matrix R_k associated with the kth eigenvector.
        In order to validate the correctness the relation

            Vor[:, :(k+1)] ~ R_1 R_2 ... R_k

        is tested. Defining W_k = R_1 R_2 ... R_k, the test reduces to

            -         -       -          -
            | . . + + |       | . .  * * |
            | . . + + |   ~   | . .  * * |
            | . . + + |       | . .  * * |
            | . . + + |       | . .  * * |
            -         -       -          -
               Vor                W_k

        where the norm of the . columns ought to be ~zero whereas the norm of
        the (+, *) columns ought to be > 0.

        """

        # setup dimensions
        full_dims = np.arange(2, self._full_dim_max)

        # test-setup: iterator on full dimensions
        for full_dim in full_dims:
            # setup a mock eigenvector and eigenvalue mtx
            A = np.eye(full_dim)
            A[:, 0] = 1.0 / np.sqrt(full_dim)
            V, _ = np.linalg.qr(A)
            E = np.arange(full_dim)[::-1]

            # test-setup: cast into an oriented basis
            (
                Vor,
                _,
                _,
                theta_matrix,
                sort_indices,
            ) = impl.orient_eigenvectors_via_arcsin_method(V, E)

            def fix(v):
                return np.round(self._rebase * v)

            # build W_k
            W_k = np.eye(full_dim)

            # test-setup: iterate across columns in (Vor, W_k)
            for cursor in np.arange(full_dim):
                k = cursor + 1

                # run-test: call focus fcxn
                W_k = W_k.dot(
                    impl.generate_oriented_eigenvectors(theta_matrix, k)
                )

                # compute norms across select matrix entries
                first_k_columns_norm = np.linalg.norm(Vor[:, :k] - W_k[:, :k])

                remaining_columns_norm = np.linalg.norm(Vor[:, k:] - W_k[:, k:])

                # test norms
                with self.subTest(
                    msg="test k-columns match - fdim {0} k {1}".format(
                        full_dim, k
                    )
                ):
                    # the first k columns match
                    self.assertEqual(0.0, fix(first_k_columns_norm))

                    # the remaining columns mismatch in general
                    # however, when there is only one rotation left, that
                    # rotation is the identity
                    #       R_(full_dim) = I
                    # so the last column of W_k will match Vor one step before
                    # k = full_dim.
                    if k < full_dim - 1:
                        self.assertNotEqual(0.0, fix(remaining_columns_norm))
                    else:
                        self.assertEqual(0.0, fix(remaining_columns_norm))
